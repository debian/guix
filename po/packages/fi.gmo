��          �      ,      �  �   �  %   ?  '   e  /   �  0   �     �       �     s       �  W  �  F  �  �  B  D   )  l   n  �   �  �  �  �   O  +   #  4   O  :   �  2   �     �         '  �  :     �  �  �    v  �  �  B   l  v   �  �   &                                     	          
                                Bluefish is an editor aimed at programmers and web developers,
with many options to write web sites, scripts and other code.
Bluefish supports many programming and markup languages. Console front-end to the GNU debugger Database independent interface for Perl Graphical front-end for GDB and other debuggers High-speed arctic racing game based on Tux Racer Internet services daemon Manage virtual machines Nload is a console application which monitors network traffic and
bandwidth usage in real time.  It visualizes the in- and outgoing traffic using
two graphs, and provides additional info like total amount of transferred data
and min/max network usage. Opus is a totally open, royalty-free, highly versatile audio codec.  Opus
is unmatched for interactive speech and music transmission over the Internet,
but is also intended for storage and streaming applications.  It is
standardized by the Internet Engineering Task Force (IETF) as RFC 6716 which
incorporated technology from Skype's SILK codec and Xiph.Org's CELT codec. SQL database server Siege is a multi-threaded HTTP/FTP load tester and benchmarking utility.  It
can stress test a single URL with a user defined number of simulated users, or
it can read many URLs into memory and stress them simultaneously.  The program
reports the total number of hits recorded, bytes transferred, response time,
concurrency, and return status. SnapRAID backs up files stored across multiple storage devices, such as
disk arrays, in an efficient way reminiscent of its namesake @acronym{RAID,
Redundant Array of Independent Disks} level 4.

Instead of creating a complete copy of the data like classic backups do, it
saves space by calculating one or more sets of parity information that's a
fraction of the size.  Each parity set is stored on an additional device the
size of the largest single storage volume, and protects against the loss of any
one device, up to a total of six.  If more devices fail than there are parity
sets, (only) the files they contained are lost, not the entire array.  Data
corruption by unreliable devices can also be detected and repaired.

SnapRAID is distinct from actual RAID in that it operates on files and creates
distinct snapshots only when run.  It mainly targets large collections of big
files that rarely change, like home media centers.  One disadvantage is that
@emph{all} data not in the latest snapshot may be lost if one device fails.  An
advantage is that accidentally deleted files can be recovered, which is not the
case with RAID.

It's also more flexible than true RAID: devices can have different sizes and
more can be added without disturbing others.  Devices that are not in use can
remain fully idle, saving power and producing less noise. The package defines a @code{tabular*}-like environment, @code{tabulary},
taking a "total width" argument as well as the column specifications.  The
environment uses column types @code{L}, @code{C}, @code{R} and @code{J} for
variable width columns (@code{\raggedright}, @code{\centering},
@code{\raggedleft}, and normally justified).  In contrast to
@code{tabularx}'s @code{X} columns, the width of each column is weighted
according to the natural width of the widest cell in the column. This package provides a dictionary for the GNU Aspell spell checker. This package provides a library and a command line
interface to the variable facility of UEFI boot firmware. This package provides all the locales supported by the GNU C Library,
more than 400 in total.  To use them set the @code{LOCPATH} environment variable
to the @code{share/locale} sub-directory of this package. Project-Id-Version: GNU guix
Report-Msgid-Bugs-To: bug-guix@gnu.org
PO-Revision-Date: 2025-01-11 09:38+0000
Last-Translator: Ricky Tigg <ricky.tigg@gmail.com>
Language-Team: Finnish <https://translate.fedoraproject.org/projects/guix/packages/fi/>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 5.9.2
 Bluefish on ohjelmoijille ja web-kehittäjille tarkoitettu muokkain, jossa on monia
vaihtoehtoja verkkosivustojen, skriptien ja muun koodin kirjoittamiseen.
Bluefish tukee monia ohjelmointi- ja merkintäkieliä. Konsolin etupää GNU-viankorjausohjelmalle Tietokannasta riippumaton käyttöliittymä Perlille Graafinen etupää GDB:lle ja muille viankorjausohjelmille Nopea arktinen ajopeli, joka perustuu Tux Raceriin Internet-palvelujen demoni Hallitse virtuaalikoneita Nload on konsolisovellus, joka valvoo verkkoliikennettä ja kaistanleveyden
käyttöä reaaliajassa. Se visualisoi saapuvan ja lähtevän liikenteen kahdella
kaaviolla ja tarjoaa lisätietoa, kuten siirretyn tiedon kokonaismäärän verkon
vähimmäis- ja enimmäiskäytön. Opus on täysin avoin, rojaltivapaa, erittäin monipuolinen ääni-koodekki. Opus on
vertaansa vailla vuorovaikutteiseen puheen ja musiikin siirtoon Internetin välityksellä,
mutta se on tarkoitettu myös tallennus- ja suoratoistosovelluksiin. Internet Engineering
Task Force (IETF) on standardoinut sen RFC 6716:ksi, joka sisälsi Skypen SILK-koodekin
ja Xiph.Orgin CELT-koodekin teknologiaa. MySQL-tietokantapalvelin Siege on monisäikeinen HTTP/FTP-kuormitustestaaja ja vertailuapuohjelma. Se voi
testata stressin suhteen yksittäisen URL-osoitteen käyttäjän määrittämän määrän
simuloituja käyttäjiä tai se voi lukea useita URL-osoitteita muistiin ja stressiä niitä
samanaikaisesti. Ohjelma raportoi tallennettujen osumien kokonaismäärän, siirretyt
tavut, vasteajan, samanaikaisuuden ja palautuksen tilan. SnapRAID varmuuskopioi useille tallennuslaitteille, kuten levyryhmille, tallennetut
tiedostot tehokkaalla tavalla, joka muistuttaa kaimansa @acronym{RAID, Redundant
Array of Independent Disks} tasoa 4.

Sen sijaan, että luodaan täydellinen kopio tiedoista, kuten perinteiset varmuuskopiot
tekevät, se säästää tilaa laskemalla yhden tai useamman pariteettitiedon joukon, joka
on murto-osa koosta. Jokainen pariteettijoukko tallennetaan lisälaitteeseen, joka on
suurimman yksittäisen tallennustilan kokoinen, ja se suojaa minkä tahansa laitteen
katoamiselta, yhteensä enintään kuusi. Jos useampi laite epäonnistuu kuin on
pariteettijoukkoja, (vain) niiden sisältämät tiedostot menetetään, ei koko joukko.
Epäluotettavien laitteiden aiheuttamat tietojen korruptio voidaan myös havaita ja korjata.

SnapRAID eroaa varsinaisesta RAIDista siinä, että se toimii tiedostoilla ja luo erilliset
tilannevedokset vain ajettaessa. Se kohdistuu pääasiassa suuriin tiedostokokoelmiin,
jotka muuttuvat harvoin, kuten kotimediakeskukset. Yksi haittapuoli on, että @emph{all}
-tiedot, jotka eivät ole viimeisimmässä tilannekuvassa, voivat kadota, jos jokin laite
epäonnistuu. Etuna on, että vahingossa poistetut tiedostot voidaan palauttaa, mikä ei
oleRAIDin tapauksessa.

Se on myös joustavampi kuin todellinen RAID: Laitteet voivat olla erikokoisia ja niitä voidaan
lisätä muita häiritsemättä. Laitteet, jotka eivät ole käytössä, voivat jäädä täysin käyttämättä,
mikä säästää virtaa ja tuottaa vähemmän melua. Paketti määrittelee @code{tabular*}-tyyppisen ympäristön, @code{tabulary},
joka ottaa "total width"-argumentin sekä sarakemääritykset. Ympäristö käyttää
saraketyyppejä @code{L}, @code{C}, @code{R} ja @code{J} muuttuvaleveisille
sarakkeille (@code{\raggedright}, @code{\centering}, @code{ \raggedleft} ja
normaalisti tasattu). Toisin kuin @code{tabularx}:n @code{X}-sarakkeissa,
jokaisen sarakkeen leveys painotetaan sarakkeen leveimmän solun luonnollisen
leveyden mukaan. Tämä paketti sisältää sanakirjan GNU Aspell oikolukua varten. Tämä paketti tarjoaa kirjaston ja komentorivikäyttöliittymän
UEFI-käynnistyslaiteohjelmiston muuttujatoimintoon. Tämä paketti sisältää kaikki GNU C -kirjaston tukemat kieliasetukset, yhteensä
yli 400. Käytä niitä asettamalla @code{LOCPATH}-ympäristömuuttuja tämän paketin
@code{share/locale}-alihakemistoon. 