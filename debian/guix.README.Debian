# Differences from "foreign distro" installations

For the most part, installing the Debian package of guix is similar to
installing guix on a foreign distro:

  https://guix.gnu.org/manual/en/guix.html#Binary-Installation

The notable differences are:

- The guix build group is _guixbuild and the users are named _guixbuilder[0-9]
  to comply with Debian policy.
- The guix-daemon and guix-publish servers are configured run from the
  binaries in /usr/bin, rather than from the "root" user's guix profile.


# Using with sysvinit

When installing on a system running sysvinit, you may need to install
the daemonize package and to either install the opensysusers package
or add the _guixbuild and _guixbuilder[0-9] users manually. To enable
the guix-daemon init script, run:

  update-rc.d guix-daemon defaults
